<atlassian-plugin key="${project.groupId}.${project.artifactId}" name="${project.name}" plugins-version="2">
    <plugin-info>
        <description>${project.description}</description>
        <version>${project.version}</version>
        <vendor name="${project.organization.name}" url="${project.organization.url}" />
    </plugin-info>

    <servlet-filter key="big-pipe-request-id" class="com.atlassian.plugin.connect.spi.http.bigpipe.BigPipeRequestIdFilter"
                    location="after-encoding">
        <url-pattern>/*</url-pattern>
    </servlet-filter>

    <servlet-filter key="iframe-resources" class="com.atlassian.plugin.connect.plugin.iframe.StaticResourcesFilter"
                    location="after-encoding">
        <url-pattern>/atlassian-connect/*</url-pattern>
    </servlet-filter>
    
    <servlet-filter key="api-scoping" class="com.atlassian.plugin.connect.plugin.module.permission.ApiScopingFilter"
            location="before-decoration">
        <url-pattern>/*</url-pattern>
    </servlet-filter>

    <servlet-filter key="oauth-2lo-authentication" class="com.atlassian.plugin.connect.plugin.module.oauth.OAuth2LOFilter"
            location="after-encoding" weight="2000">
        <url-pattern>/*</url-pattern>
    </servlet-filter>

    <servlet-filter key="big-pipe-content" class="com.atlassian.plugin.connect.spi.http.bigpipe.BigPipeContentFilter"
                    location="before-dispatch">
        <url-pattern>/bigpipe/request/*</url-pattern>
    </servlet-filter>

    <servlet-filter key="servlet-context-thread-local-filter" name="Servlet Context Thread Local Filter"
                    class="com.atlassian.core.filters.ServletContextThreadLocalFilter"
                    location="before-login" weight="100">
        <url-pattern>*</url-pattern>
    </servlet-filter>

    <servlet key="remotablePlugins2LORedirectingServlet"
             name="Remotable Plugins 2-Legged OAuth Signing and Redirecting Servlet"
             class="com.atlassian.plugin.connect.plugin.module.util.redirect.RedirectServlet">
        <description>Redirects to Remotable Plugins, using 2-legged OAuth to sign the outgoing request.  Request parameters are included in the redirect.</description>
        <url-pattern>/redirect/oauth</url-pattern>
    </servlet>
    <servlet key="remotablePluginsPermanentRedirectingServlet"
             name="Remotable Plugins Permanent Redirecting Servlet"
             class="com.atlassian.plugin.connect.plugin.module.util.redirect.RedirectServlet">
        <description>
            Redirects to a Remote App resource, usually an image, with a permanent redirect, but
            doesn't sign the request.  Request parameters are included in the redirect.
        </description>
        <url-pattern>/redirect/permanent</url-pattern>
    </servlet>
    <servlet key="remotablePluginsIframe"
             name="Remotable Plugins Iframe Generator"
             class="com.atlassian.plugin.connect.plugin.module.page.ContextFreeIFramePageServlet">
        <description>
            Generates a template for serving a signed remote iframe
        </description>
        <url-pattern>/render-signed-iframe</url-pattern>
    </servlet>

    <web-resource key="ap-amd">
        <resource type="download" name="_ap.js" location="/js/iframe/host/_ap.js" />
        <resource type="download" name="_amd.js" location="/js/iframe/_amd.js" />
        <resource type="download" name="_dollar.js" location="/js/iframe/host/_dollar.js" />
    </web-resource>

    <web-resource key="iframe-host-js">
        <dependency>com.atlassian.auiplugin:ajs</dependency>
        <dependency>com.atlassian.plugins.atlassian-connect-plugin:ap-amd</dependency>
        <resource type="download" name="_events.js" location="/js/iframe/_events.js" />
        <resource type="download" name="_xdm.js" location="/js/iframe/_xdm.js" />
        <resource type="download" name="_addons.js" location="/js/iframe/host/_addons.js" />
        <resource type="download" name="main.js" location="/js/iframe/host/main.js" />
    </web-resource>

    <!-- Temporary copy of AUI dialog2 before it lands in all products -->
    <web-resource key="ajs-dialog2">
        <resource type="download" name="layer.css" location="css/auidialog2/layer.css" />
        <resource type="download" name="dialog2.css" location="css/auidialog2/dialog2.css" />
        <resource type="download" name="ap-ajs.js" location="js/auidialog2/ap-ajs.js" />
        <resource type="download" name="browser.js" location="js/auidialog2/internal/browser.js" />
        <resource type="download" name="widget.js" location="js/auidialog2/internal/widget.js" />
        <resource type="download" name="focus-manager.js" location="js/auidialog2/focus-manager.js" />
        <resource type="download" name="layer-manager.js" location="js/auidialog2/layer-manager.js" />
        <resource type="download" name="layer-manager-global.js" location="js/auidialog2/layer-manager-global.js" />
        <resource type="download" name="layer.js" location="js/auidialog2/layer.js" />
        <resource type="download" name="dialog2.js" location="js/auidialog2/dialog2.js" />
        <dependency>com.atlassian.auiplugin:ajs</dependency>
        <dependency>com.atlassian.plugins.atlassian-connect-plugin:ap-amd</dependency>
    </web-resource>

    <web-resource key="dialog">
        <resource type="download" name="dialog.css" location="css/dialog/dialog.css" />
        <resource type="download" name="main.js" location="js/dialog/main.js" />
        <resource type="download" name="simple.js" location="js/dialog/simple.js" />
        <dependency>com.atlassian.auiplugin:ajs</dependency>
        <dependency>com.atlassian.plugins.atlassian-connect-plugin:ap-amd</dependency>
        <dependency>com.atlassian.plugins.atlassian-connect-plugin:ajs-dialog2</dependency>
    </web-resource>

    <web-resource key="confluence-macro">
        <resource type="download" name="editor.js" location="js/confluence/macro/editor.js" />
        <dependency>com.atlassian.auiplugin:ajs</dependency>
        <dependency>com.atlassian.plugins.atlassian-connect-plugin:ap-amd</dependency>
    </web-resource>

    <web-resource key="iframe-host-css">
        <resource type="download" name="loading.gif" location="/css/iframe/loading.gif" />
        <resource type="download" name="host.css" location="/css/iframe/host.css" />
    </web-resource>

    <web-resource key="remote-condition">
        <resource type="download" name="remote.js" location="js/condition/remote.js" />
        <dependency>com.atlassian.plugins.atlassian-connect-plugin:ap-amd</dependency>
        <dependency>com.atlassian.plugins.atlassian-connect-plugin:big-pipe</dependency>
    </web-resource>

    <web-resource key="images">
        <resource type="download" name="images/" location="images/" />
    </web-resource>

    <web-resource key="dialog-page-resource">
        <resource type="download" name="dialog-binder.js" location="/js/dialog/binder.js" />
        <dependency>com.atlassian.auiplugin:ajs</dependency>
        <dependency>com.atlassian.plugins.atlassian-connect-plugin:dialog</dependency>
    </web-resource>

    <web-resource key="schema-xsl">
        <resource type="download" content-type="application/xml" name="xs3p.xsl" location="/xsd/xs3p.xsl"/>
    </web-resource>

    <!-- big pipe stuff -->
    <web-resource key="big-pipe">
        <resource type="download" name="bigpipe.js" location="js/bigpipe/bigpipe.js" />
        <dependency>com.atlassian.auiplugin:ajs</dependency>
        <dependency>com.atlassian.plugins.atlassian-connect-plugin:ap-amd</dependency>
    </web-resource>

    <web-resource key="atlassian-connect-resources" name="Connect resources for inclusion on every page.">
        <context>atl.admin</context>
        <context>atl.general</context>
        <!-- workaround for AC-703, the 'preview' view does not include atl.general resources (CONF-30617) -->
        <context>preview</context>
        <dependency>com.atlassian.plugins.atlassian-connect-plugin:big-pipe</dependency>
        <dependency>com.atlassian.plugins.atlassian-connect-plugin:dialog-page-resource</dependency>
        <dependency>com.atlassian.plugins.atlassian-connect-plugin:remote-condition</dependency>
        <dependency>com.atlassian.plugins.atlassian-connect-plugin:iframe-host-css</dependency>
        <dependency>com.atlassian.plugins.atlassian-connect-plugin:confluence-macro</dependency>
        <dependency>com.atlassian.plugins.atlassian-connect-plugin:iframe-host-js</dependency>
        <dependency>com.atlassian.plugins.atlassian-connect-plugin:ap-amd</dependency>
    </web-resource>

    <component key="pluginUriResolver"
               interface="com.atlassian.webhooks.spi.plugin.PluginUriResolver" public="true"
               class="com.atlassian.plugin.connect.plugin.module.webhook.RemotablePluginsPluginUriResolver" />

    <component key="httpContentRetriever" class="com.atlassian.plugin.connect.plugin.util.http.CachingHttpContentRetriever"
               public="true" interface="com.atlassian.plugin.connect.plugin.util.http.HttpContentRetriever" />

    <component key="startableForPlugins" class="com.atlassian.plugin.connect.plugin.integration.plugins.StartableForPlugins" public="true"
               interface="com.atlassian.sal.api.lifecycle.LifecycleAware" />

    <component key="productEventPublisher" class="com.atlassian.plugin.connect.plugin.event.ProductEventPublisher"
               public="true" interface="com.atlassian.sal.api.lifecycle.LifecycleAware" />

    <component key="connectIdentifier" class="com.atlassian.plugin.connect.plugin.service.DefaultConnectAddOnIdentifierService"
               interface="com.atlassian.plugin.connect.spi.ConnectAddOnIdentifierService" public="true" />

    <component key="upmInstallHandler" class="com.atlassian.plugin.connect.plugin.installer.ConnectUPMInstallHandler"
               interface="com.atlassian.upm.spi.PluginInstallHandler" public="true" />

    <component key="remotePluginRequestSigner" class="com.atlassian.plugin.connect.plugin.webhooks.RemotePluginRequestSigner"
               interface="com.atlassian.webhooks.spi.plugin.RequestSigner" public="true" />

    <component key="serverWebHookProvider"
               interface="com.atlassian.webhooks.spi.provider.WebHookProvider" public="true"
               class="com.atlassian.plugin.connect.plugin.webhooks.ServerWebHookProvider" />

    <component key="pluginsWebHookProvider"
                   interface="com.atlassian.webhooks.spi.provider.WebHookProvider" public="true"
                   class="com.atlassian.plugin.connect.plugin.webhooks.PluginsWebHookProvider" />

    <component key="bundleLocator" class="com.atlassian.plugin.connect.plugin.util.BundleContextBundleLoader" />
    
    <component key="permissionsReader" class="com.atlassian.plugin.connect.plugin.descriptor.DescriptorPermissionsReader"
               interface="com.atlassian.plugin.connect.spi.permission.PermissionsReader" public="true"/>

    <component key="bigPipeManager" class="com.atlassian.plugin.connect.spi.http.bigpipe.DefaultBigPipeManager"
               interface="com.atlassian.plugin.connect.api.service.http.bigpipe.BigPipeManager" />

    <component-import key="templateRenderer" interface="com.atlassian.templaterenderer.TemplateRenderer"/>
    <component-import key="servletModuleManager" interface="com.atlassian.plugin.servlet.ServletModuleManager" />
    <component-import key="webInterfaceManager" interface="com.atlassian.plugin.web.WebInterfaceManager" />
    <component-import key="pluginRetrievalService" interface="com.atlassian.plugin.osgi.bridge.external.PluginRetrievalService" />
    <component-import key="oauthConsumerService" interface="com.atlassian.oauth.consumer.ConsumerService" />
    <component-import key="requestFactory" interface="com.atlassian.sal.api.net.RequestFactory" />
    <component-import key="pluginController" interface="com.atlassian.plugin.PluginController" />
    <component-import key="applicationProperties" interface="com.atlassian.sal.api.ApplicationProperties" />
    <component-import key="serviceProviderConsumerStore" interface="com.atlassian.oauth.serviceprovider.ServiceProviderConsumerStore" />
    <component-import key="applicationLinkService" interface="com.atlassian.applinks.spi.link.MutatingApplicationLinkService" />
    <component-import key="authenticationConfigurationManager" interface="com.atlassian.applinks.spi.auth.AuthenticationConfigurationManager" />
    <component-import key="userManager" interface="com.atlassian.sal.api.user.UserManager" />
    <component-import key="authenticationListener" interface="com.atlassian.sal.api.auth.AuthenticationListener" />
    <component-import key="authenticationController" interface="com.atlassian.sal.api.auth.AuthenticationController" />
    <component-import key="transactionTemplate" interface="com.atlassian.sal.api.transaction.TransactionTemplate" />
    <component-import key="pluginEventManager" interface="com.atlassian.plugin.event.PluginEventManager" />
    <component-import key="pluginAccessor" interface="com.atlassian.plugin.PluginAccessor" />
    <component-import key="webResourceManager" interface="com.atlassian.plugin.webresource.WebResourceManager" />
    <component-import key="eventPublisher" interface="com.atlassian.event.api.EventPublisher" />
    <component-import key="i18nResolver" interface="com.atlassian.sal.api.message.I18nResolver" />
    <component-import key="localeResolver" interface="com.atlassian.sal.api.message.LocaleResolver" />
    <component-import key="pluginSettingsFactory" interface="com.atlassian.sal.api.pluginsettings.PluginSettingsFactory" />
    <component-import key="typeAccessor" interface="com.atlassian.applinks.spi.util.TypeAccessor" />
    <component-import key="webResourceUrlProvider" interface="com.atlassian.plugin.webresource.WebResourceUrlProvider" />
    <component-import key="moduleFactory" interface="com.atlassian.plugin.module.ModuleFactory" />
    <component-import key="httpClientFactory" interface="com.atlassian.httpclient.api.factory.HttpClientFactory" />
    <component-import key="httpClient" interface="com.atlassian.httpclient.api.HttpClient" />
    <component-import key="moduleDescriptorWebHookRegistry"
                      interface="com.atlassian.webhooks.spi.provider.ModuleDescriptorWebHookListenerRegistry" />
    <component-import key="remotePluginLicenseService" interface="com.atlassian.upm.api.license.RemotePluginLicenseService" />

    <component key="userIsSysAdminCondition" class="com.atlassian.plugin.connect.spi.module.UserIsSysAdminCondition" />
    <component key="userIsAdminCondition" class="com.atlassian.plugin.connect.spi.module.UserIsAdminCondition" />

    <applinks-application-type name="Remote Plugin Container" key="remote-plugin-container-type"
                                   class="com.atlassian.plugin.connect.plugin.module.applinks.RemotePluginContainerApplicationTypeImpl"
                                   interface="com.atlassian.plugin.connect.spi.applinks.RemotePluginContainerApplicationType">
            <manifest-producer class="com.atlassian.plugin.connect.plugin.module.applinks.RemotePluginContainerManifestProducer"/>
    </applinks-application-type>

    <!-- remote extension points -->

    <described-module-type key="remote-plugin-container" name="Remote Plugin Container"
                               class="com.atlassian.plugin.connect.plugin.module.applinks.RemotePluginContainerModuleDescriptor">
        <description>The server that is hosting this plugin</description>
        <optional-permissions>
            <permission>create_oauth_link</permission>
        </optional-permissions>
    </described-module-type>

    <described-module-type key="general-page" class="com.atlassian.plugin.connect.plugin.module.page.GeneralPageModuleDescriptor">
        <description>
            A full-sized page that will be decorated as a general page, meaning it will receive the product header
            and footer.  A link will be created in a default location in the header.  Viewing the page allowed
            for both authenticated and anonymous users.
        </description>
    </described-module-type>

    <described-module-type key="admin-page" name="Administration Page" class="com.atlassian.plugin.connect.plugin.module.page.AdminPageModuleDescriptor">
        <description>
            A full-sized page that will be decorated as a product administration page.  A link will be created
            in a default location in the administration menu.  Viewing the page will be restricted to product
            administrators.
        </description>
    </described-module-type>

    <described-module-type key="configure-page" class="com.atlassian.plugin.connect.plugin.module.page.ConfigurePageModuleDescriptor" max-occurs="1">
        <description>
            A full-sized page that will be decorated as a product administration page.  A link to the page
            will show up in the plugin's details page in the Add-On Manager.  Viewing the page will be restricted to product
            administrators.
        </description>
    </described-module-type>

    <described-module-type key="dialog-page" class="com.atlassian.plugin.connect.plugin.module.page.dialog.DialogPageModuleDescriptor">
        <description>Loads a remote URL (iframe-wrapped) in an AUI Dialog</description>
    </described-module-type>

    <described-module-type key="plugin-permission" name="Plugin Permission"
                                   class="com.atlassian.plugin.connect.plugin.module.permission.DefaultPermissionModuleDescriptor">
        <description>A permission that needs to be requested by a plugin in order to execute certain functionality</description>
    </described-module-type>

    <described-module-type key="remote-web-panel" class="com.atlassian.plugin.connect.plugin.module.webpanel.RemoteWebPanelModuleDescriptor">
        <description>
            A remote page decorated as a web panel on the project configuration page.
        </description>
    </described-module-type>

    <described-module-type key="remote-web-item" class="com.atlassian.plugin.connect.plugin.module.webitem.RemoteWebItemModuleDescriptor">
        <description>
            Relative link leading to a remote page decorated as remote page, or an absolute link leading outside of a product.
        </description>
    </described-module-type>

    <plugin-permission key="insert_web_resources" name="Insert Web Resources">
        <description>Ability to insert web resources on pages of the product.</description>
    </plugin-permission>

    <plugin-permission key="send_email" name="Send Email"
                       class="com.atlassian.plugin.connect.plugin.product.SendEmailScope">
        <description>Ability to send an email from the Atlassian server</description>
    </plugin-permission>

    <plugin-permission key="make_http_requests" name="Make HTTP requests">
        <description>Ability to make HTTP requests from the Atlassian server</description>
    </plugin-permission>

    <plugin-permission key="intercept_requests" name="Intercept Requests">
        <description>Ability to intercept requests coming into the Atlassian server.</description>
    </plugin-permission>

    <plugin-permission key="store_data" name="Store Data">
        <description>Ability to store data on the Atlassian server</description>
    </plugin-permission>

    <plugin-permission key="use_db_connection" name="Use DB connection">
        <description>Ability to use the DB connection provided by the Atlassian server.</description>
    </plugin-permission>

    <plugin-permission key="access_filesystem" name="Access File System">
        <description>Ability to access the filesystem on the Atlassian server.</description>
    </plugin-permission>

    <plugin-permission key="generate_any_html" name="Generate Any HTML">
        <description>Ability to generate any HTML without any sanitisation.</description>
    </plugin-permission>

    <plugin-permission key="use_reflection" name="Use Reflection">
        <description>Ability to use reflection on the Atlassian server.</description>
    </plugin-permission>

    <plugin-permission key="create_classloaders" name="Create Class Loaders">
        <description>Ability to create class loaders on the Atlassian server.</description>
    </plugin-permission>

    <plugin-permission key="create_oauth_link" name="Create OAuth Link">
        <description>Ability to create an incoming OAuth link that will allow trusted access</description>
    </plugin-permission>

    <plugin-permission key="read_app_links" name="Read Application Links"
                       class="com.atlassian.plugin.connect.plugin.product.ReadAppLinksScope">
        <description>Allows a Remotable Plugin to retrieve the host application's configured application links and entity links.</description>
    </plugin-permission>

    <plugin-permission key="modify_app_link" name="Modify Owned App Link"
                       class="com.atlassian.plugin.connect.plugin.product.ModifyAppLinkScope">
        <description>Allows a Remotable Plugin to modify the details of its own configured Application Link.</description>
    </plugin-permission>

    <plugin-permission key="read_license" name="Read License"
                       class="com.atlassian.plugin.connect.plugin.product.ReadLicenseScope">
        <description>Allows a Remotable Plugin to retrieve the information about its license.</description>
    </plugin-permission>

    <plugin-permission key="define_plugin_permission" name="Define Plugin Permission">
        <description>Ability to define new plugin permissions.</description>
    </plugin-permission>

    <!-- product-specific modules included via xslt build process -->
</atlassian-plugin>
