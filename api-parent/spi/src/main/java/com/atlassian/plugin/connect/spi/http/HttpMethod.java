package com.atlassian.plugin.connect.spi.http;

/**
 * Represents HTTP methods.
 *
 * @since 0.10
 */
public enum HttpMethod
{
    GET,
    POST,
    PUT,
    DELETE,
    HEAD,
    TRACE
}