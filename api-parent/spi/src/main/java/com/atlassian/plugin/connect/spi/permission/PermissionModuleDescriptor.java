package com.atlassian.plugin.connect.spi.permission;

import com.atlassian.plugin.ModuleDescriptor;

/**
 * Interface for retrieval of permission plugin modules
 */
public interface PermissionModuleDescriptor extends ModuleDescriptor<Permission>
{
}
