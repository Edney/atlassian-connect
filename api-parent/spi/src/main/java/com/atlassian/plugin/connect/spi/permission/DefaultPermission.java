package com.atlassian.plugin.connect.spi.permission;

public final class DefaultPermission extends AbstractPermission
{
    public DefaultPermission(String key, PermissionInfo permissionInfo)
    {
        super(key, permissionInfo);
    }
}
