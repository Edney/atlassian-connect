package it;

public class TestConstants
{
    public static final String ADMIN_FULL_NAME = "A. D. Ministrator (Sysadmin)";
    public static final String ADMIN_USERNAME = "admin";
    public static final String BETTY_USERNAME = "betty";

    public static final String IFRAME_ID_SUFFIX = "_provider";
}
